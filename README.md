# Tugas Visionet #

* Tugas tgl 20 Oktober 2018
* Membuat 2 Recyclerview horizontal dan vertical di dalam 1 fragment dan datanya mengambil dari API
* Build with Kotlin, Use MVVM Pattern

List Library Yang Dipakai:
* Recyclerview
* Cardview
* Retrofit
* RxJava
* Picasso

Refrensi API :
* https://www.themealdb.com/api/json/v1/1/latest.php
* https://www.themealdb.com/api/json/v1/1/categories.php

Screenhot :
<br>
screenshot kiri adalah saat aplikasi sukses request API dan yg kanan adalah aplikasi yang gagal request di karenakan tidak ada koneksi.
<br>
![](https://lh4.googleusercontent.com/RUW9EAzbe4BBwW7Ki3COKQsnAontXfc3cxokZTcKq4ab5qmR3ANrfbxUoH-2sEwEyAfUrpA_4ZNE5jvwQEE=w1366-h636-rw)
![](https://lh5.googleusercontent.com/Ydxu3usW19-7Hy1ayunLLKZbVcmfmIwyZ7Bl8-HSuWlCLtUb-Wo-XQrTJQg4pKt8PIg82-y8PG93-e88n2w=w1366-h636-rw)

<br>

    Terimakasih visionet telah mengajari kami dan memberikan kami submission 
    agar lebih sering praktek dan memperdalam ilmu mobile development.
    .
    .
    .
    Copyright 2018 Haidar Zamzam