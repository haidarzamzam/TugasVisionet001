package com.haidar.root.tugasvisionet001.bases

interface BaseViewModel {

    fun onCreate()
    fun onPause()
    fun onResume()
    fun onDestroy()
}