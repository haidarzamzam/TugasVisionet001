package com.haidar.root.tugasvisionet001.main.horizontal.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.haidar.root.tugasvisionet001.R
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerHorizontalBinding
import com.haidar.root.tugasvisionet001.main.horizontal.models.ItemRecyclerHorizontalModel
import com.haidar.root.tugasvisionet001.main.horizontal.viewmodels.ItemRecyclerHorizontalViewModel

class RecyclerHorizontalAdapter(private val context: Context, private val myRecyclerList:MutableList<ItemRecyclerHorizontalModel>): RecyclerView.Adapter<RecyclerHorizontalAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val recyclerHorizontalBinding: ItemRecyclerHorizontalBinding = DataBindingUtil.inflate(LayoutInflater.from(parent?.context), R.layout.item_recycler_horizontal, parent, false)
        return MyViewHolder(
            recyclerHorizontalBinding.root,
            recyclerHorizontalBinding
        )
    }

    override fun getItemCount(): Int {
        return myRecyclerList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val fixPosition = holder.adapterPosition
        holder.setupData(context, myRecyclerList[fixPosition])
    }

    class MyViewHolder (val view: View, val recyclerHorizontalBinding: ItemRecyclerHorizontalBinding) : RecyclerView.ViewHolder(view) {
        fun setupData(context: Context, myRecyclerList: ItemRecyclerHorizontalModel){
            val itemRecyclerHorizontalViewModel =
                ItemRecyclerHorizontalViewModel(
                    context,
                    myRecyclerList,
                    recyclerHorizontalBinding
                )
            recyclerHorizontalBinding.itemHorizontal = itemRecyclerHorizontalViewModel
            itemRecyclerHorizontalViewModel.setData()
        }

    }
}