package com.haidar.root.tugasvisionet001.main.horizontal.viewmodels

import android.content.Context
import android.databinding.ObservableField
import android.view.View
import android.widget.Toast
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerHorizontalBinding
import com.haidar.root.tugasvisionet001.main.horizontal.models.ItemRecyclerHorizontalModel
import com.squareup.picasso.Picasso

class ItemRecyclerHorizontalViewModel (val context: Context, val myRecyclerList: ItemRecyclerHorizontalModel, val binding: ItemRecyclerHorizontalBinding) {

    var nama: ObservableField<String> = ObservableField()
    var lokasi: ObservableField<String> = ObservableField()

    fun setData(){
        nama.set(myRecyclerList.nama)
        lokasi.set(myRecyclerList.lokasi)

        Picasso.with(context)
            .load(myRecyclerList.image)
            .into(binding.ivImage)
    }

    fun onClickItem(view:View){
        Toast.makeText(context, "Click : ${myRecyclerList.nama}", Toast.LENGTH_SHORT).show()
    }
}