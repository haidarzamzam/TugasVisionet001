package com.haidar.root.tugasvisionet001.main.horizontal.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.haidar.root.tugasvisionet001.R
import com.haidar.root.tugasvisionet001.databinding.FragmentRecyclerHorizontalBinding
import com.haidar.root.tugasvisionet001.main.horizontal.adapter.RecyclerHorizontalAdapter
import com.haidar.root.tugasvisionet001.main.horizontal.models.ItemRecyclerHorizontalModel
import com.haidar.root.tugasvisionet001.main.horizontal.viewmodels.RecyclerHorizontalViewModel

class RecyclerHorizontalFragment:Fragment() {

    private lateinit var recyclerHorizontalBinding: FragmentRecyclerHorizontalBinding
    private lateinit var vmRecyclerHorizontal : RecyclerHorizontalViewModel

    private lateinit var adapter: RecyclerHorizontalAdapter
    private var listData:MutableList<ItemRecyclerHorizontalModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        recyclerHorizontalBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recycler_horizontal, container,false)
        vmRecyclerHorizontal = RecyclerHorizontalViewModel()
        recyclerHorizontalBinding.recyclerHorizontal = vmRecyclerHorizontal

        startRecyclerView()
        return recyclerHorizontalBinding.root
    }

    private fun startRecyclerView() {
        listData.add(
            ItemRecyclerHorizontalModel(
                "Alun Alun Malang",
                "Kota Malang",
                R.drawable.thumbnail_alun_alun_malang
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Alun alun Tugu",
                "Kota Malang",
                R.drawable.thumbnail_alun_alun_tugu_malang
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Alun Alun Batu",
                "Kota Batu",
                R.drawable.thumbnail_alun_alun_batu
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Gunung Bromo",
                "Kab. Malang",
                R.drawable.thumbnail_gunung_bromo
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Museum Angkut",
                "Kota Batu",
                R.drawable.thumbnail_museum_angkut
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Pantai Sendang Biru",
                "Kab. Malang",
                R.drawable.thumbnail_sendang_biru
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "BNS (Batu Night Spectacular)",
                "Kota Batu",
                R.drawable.thumbnail_bns
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Kebun Teh Wonosari",
                "Kab. Malang",
                R.drawable.thumbnail_kebun_teh_wonosari
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Wisata Paralayang",
                "Kota Batu",
                R.drawable.thumbnail_paralayang
            )
        )
        listData.add(
            ItemRecyclerHorizontalModel(
                "Coban Pelangi",
                "Kab. Malang",
                R.drawable.thumbnail_coban_pelangi
            )
        )
        val rv = recyclerHorizontalBinding.rvItemHorizontal
        rv.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL,false)
        adapter =
                RecyclerHorizontalAdapter(context!!, listData)
        rv.adapter = adapter
    }
}