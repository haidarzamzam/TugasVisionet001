package com.haidar.root.tugasvisionet001.main.vertical.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.haidar.root.tugasvisionet001.R
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerCategoryBinding
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel
import com.haidar.root.tugasvisionet001.main.vertical.viewmodels.ItemRecyclerCategoryViewModel

class RecyclerCategoryAdapter(private val context: Context, private val myRecyclerList:MutableList<ItemRecyclerCategoryModel.DataCategory>): RecyclerView.Adapter<RecyclerCategoryAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerCategoryAdapter.MyViewHolder {
        val recyclerCategoryBinding: ItemRecyclerCategoryBinding = DataBindingUtil.inflate(LayoutInflater.from(parent?.context), R.layout.item_recycler_category, parent, false)
        return MyViewHolder(recyclerCategoryBinding.root, recyclerCategoryBinding)
    }

    fun addData(mutableList: MutableList<ItemRecyclerCategoryModel.DataCategory>){
        this.myRecyclerList.clear()
        this.myRecyclerList.addAll(mutableList)
    }

    override fun getItemCount(): Int {
        return myRecyclerList.size
    }

    override fun onBindViewHolder(holder: RecyclerCategoryAdapter.MyViewHolder, position: Int) {
        val fixPosition = holder?.adapterPosition
        holder.setupData(context, myRecyclerList[fixPosition!!])
    }

    class MyViewHolder (val view: View, val recyclerCategoryBinding: ItemRecyclerCategoryBinding) : RecyclerView.ViewHolder(view) {
        fun setupData(context: Context, myRecyclerList: ItemRecyclerCategoryModel.DataCategory){
            val itemRecyclerVerticalViewModel = ItemRecyclerCategoryViewModel(context, myRecyclerList, recyclerCategoryBinding)
            recyclerCategoryBinding.itemCategory = itemRecyclerVerticalViewModel
            itemRecyclerVerticalViewModel.setData()
        }
    }
}