package com.haidar.root.tugasvisionet001.main.vertical.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.haidar.root.tugasvisionet001.R
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerMealsBinding
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel
import com.haidar.root.tugasvisionet001.main.vertical.viewmodels.ItemRecyclerMealsViewModel

class RecyclerMealsAdapter(private val context: Context, private val myRecyclerList:MutableList<ItemRecyclerMealsModel.DataMeals>): RecyclerView.Adapter<RecyclerMealsAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerMealsAdapter.MyViewHolder {
        val recyclerMealsBinding: ItemRecyclerMealsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent?.context), R.layout.item_recycler_meals, parent, false)
        return MyViewHolder(recyclerMealsBinding.root, recyclerMealsBinding)
    }

    fun addData(mutableList: MutableList<ItemRecyclerMealsModel.DataMeals>){
        this.myRecyclerList.clear()
        this.myRecyclerList.addAll(mutableList)
    }

    override fun getItemCount(): Int {
        return myRecyclerList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val fixPosition = holder?.adapterPosition
        holder.setupData(context, myRecyclerList[fixPosition!!])
    }

    class MyViewHolder (val view: View, val recyclerMealsBinding: ItemRecyclerMealsBinding) : RecyclerView.ViewHolder(view) {
        fun setupData(context: Context, myRecyclerList: ItemRecyclerMealsModel.DataMeals){
            val itemRecyclerVerticalViewModel = ItemRecyclerMealsViewModel(context, myRecyclerList, recyclerMealsBinding)
            recyclerMealsBinding.itemMeals = itemRecyclerVerticalViewModel
            itemRecyclerVerticalViewModel.setData()
        }
    }
}