package com.haidar.root.tugasvisionet001.main.vertical.interfaces

import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel

interface IvCategoryRepository {
    fun onGetCategorySuccess(listTripModel: ItemRecyclerCategoryModel)
    fun onGetCategoryError()
}