package com.haidar.root.tugasvisionet001.main.vertical.interfaces

import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel

interface IvMealsRepository {
    fun onGetMealsSuccess(listTripModel: ItemRecyclerMealsModel)
    fun onGetMealsError()
}