package com.haidar.root.tugasvisionet001.main.vertical.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ItemRecyclerCategoryModel (

    @Expose @SerializedName("categories") val categories:List<DataCategory>) {

    class DataCategory(
        @Expose @SerializedName("idCategory") val idCategory: String,
        @Expose @SerializedName("strCategory") val strCategory: String,
        @Expose @SerializedName("strCategoryThumb") val strCategoryThumb: String,
        @Expose @SerializedName("strCategoryDescription") val strCategoryDescription: String
    )
}