package com.haidar.root.tugasvisionet001.main.vertical.viewmodels

import android.content.Context
import android.databinding.ObservableField
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerCategoryBinding
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel
import com.squareup.picasso.Picasso

class ItemRecyclerCategoryViewModel(val context: Context, val myRecyclerList: ItemRecyclerCategoryModel.DataCategory, val binding: ItemRecyclerCategoryBinding) {
    var categoryName: ObservableField<String> = ObservableField()

    fun setData(){
        categoryName.set(myRecyclerList.strCategory)

        Picasso.with(context)
            .load(myRecyclerList.strCategoryThumb)
            .into(binding.ivImage)
    }
}