package com.haidar.root.tugasvisionet001.main.vertical.viewmodels

import android.content.Context
import android.databinding.ObservableField
import android.view.View
import android.widget.Toast
import com.haidar.root.tugasvisionet001.databinding.ItemRecyclerMealsBinding
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel
import com.squareup.picasso.Picasso

class ItemRecyclerMealsViewModel(val context: Context, val myRecyclerList: ItemRecyclerMealsModel.DataMeals, val binding: ItemRecyclerMealsBinding) {

    var nama: ObservableField<String> = ObservableField()
    var lokasi: ObservableField<String> = ObservableField()

    fun setData(){
        nama.set(myRecyclerList.strMeal)
        lokasi.set(myRecyclerList.strCategory)

        Picasso.with(context)
            .load(myRecyclerList.strMealThumb)
            .into(binding.ivImage)
    }

    fun onClickItem(view: View){
        Toast.makeText(context, "Click : ${myRecyclerList.strMeal}", Toast.LENGTH_SHORT).show()
    }
}