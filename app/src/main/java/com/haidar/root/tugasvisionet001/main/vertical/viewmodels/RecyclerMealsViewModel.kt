package com.haidar.root.tugasvisionet001.main.vertical.viewmodels

import com.haidar.root.tugasvisionet001.bases.BaseViewModel
import com.haidar.root.tugasvisionet001.main.vertical.interfaces.IvCategoryRepository
import com.haidar.root.tugasvisionet001.main.vertical.interfaces.IvMealsRepository
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel
import com.haidar.root.tugasvisionet001.networks.CategoryResponse
import com.haidar.root.tugasvisionet001.networks.MealsResponse
import com.haidar.root.tugasvisionet001.networks.ResponseRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RecyclerMealsViewModel(private val ivMealsRepository: IvMealsRepository, private val ivCategoryRepository: IvCategoryRepository) : BaseViewModel {

    private val listMealsResponse: MealsResponse = ResponseRepository.provideListMealsRepository()
    private val listCategoryResponse: CategoryResponse = ResponseRepository.provideListCategoryRepository()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate() {
        requestGetMeals()
    }

    override fun onPause() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onResume() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroy() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun requestGetMeals(){
        compositeDisposable.add(
            listMealsResponse.getListMeals()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                        t: ItemRecyclerMealsModel? -> ivMealsRepository.onGetMealsSuccess(t!!)
                }, { _: Throwable? ->  ivMealsRepository.onGetMealsError() })
        )
    }

    fun requestGetCategory(){
        compositeDisposable.add(
            listCategoryResponse.getListTrip()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    t:ItemRecyclerCategoryModel? -> ivCategoryRepository.onGetCategorySuccess(t!!)
                },{
                    _: Throwable? -> ivCategoryRepository.onGetCategoryError()
                })
        )
    }
}