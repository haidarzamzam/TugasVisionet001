package com.haidar.root.tugasvisionet001.main.vertical.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.haidar.root.tugasvisionet001.R
import com.haidar.root.tugasvisionet001.databinding.FragmentRecyclerMealsBinding
import com.haidar.root.tugasvisionet001.main.vertical.adapter.RecyclerMealsAdapter
import com.haidar.root.tugasvisionet001.main.vertical.adapter.RecyclerCategoryAdapter
import com.haidar.root.tugasvisionet001.main.vertical.interfaces.IvCategoryRepository
import com.haidar.root.tugasvisionet001.main.vertical.interfaces.IvMealsRepository
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel
import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel
import com.haidar.root.tugasvisionet001.main.vertical.viewmodels.RecyclerMealsViewModel
import com.haidar.root.tugasvisionet001.utils.ApplicationHelper

class RecyclerMealsFragment:Fragment(), IvMealsRepository, IvCategoryRepository {
    private lateinit var recyclerVerticalBinding: FragmentRecyclerMealsBinding
    private lateinit var vmRecyclerVertical: RecyclerMealsViewModel

    private lateinit var adapterMeals: RecyclerMealsAdapter
    private var listMeals:MutableList<ItemRecyclerMealsModel.DataMeals> = arrayListOf()

    private lateinit var adapterCategory: RecyclerCategoryAdapter
    private var listCategory:MutableList<ItemRecyclerCategoryModel.DataCategory> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        recyclerVerticalBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recycler_meals, container, false)
        vmRecyclerVertical = RecyclerMealsViewModel(this,this)
        recyclerVerticalBinding.recyclerVertical = vmRecyclerVertical

        vmRecyclerVertical.requestGetMeals()
        vmRecyclerVertical.requestGetCategory()
        startRecyclerCategory()
        startRecyclerMeals()
        initSwipeRefresh()
        return recyclerVerticalBinding.root
    }

    private fun initSwipeRefresh() {
        recyclerVerticalBinding.swipeRefreshListTrip.setColorSchemeColors(
                ContextCompat.getColor(context!!, R.color.colorPrimary),
        ContextCompat.getColor(context!!, R.color.colorPrimaryDark)
        )
        recyclerVerticalBinding.swipeRefreshListTrip.isRefreshing = true
        recyclerVerticalBinding.swipeRefreshListTrip.setOnRefreshListener {
            vmRecyclerVertical.requestGetCategory()
        }
    }

    private fun startRecyclerCategory() {
        val rv = recyclerVerticalBinding.rvItemHor
        rv.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL,false)
        adapterCategory = RecyclerCategoryAdapter(context!!, listCategory)
        rv.adapter = adapterCategory
    }

    private fun startRecyclerMeals() {
        val rv = recyclerVerticalBinding.rvItemVer
        rv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL,false)
        adapterMeals = RecyclerMealsAdapter(context!!, listMeals)
        rv.adapter = adapterMeals
    }

    override fun onGetCategorySuccess(listTripModel: ItemRecyclerCategoryModel) {
        recyclerVerticalBinding.swipeRefreshListTrip.isRefreshing = false
        adapterCategory.addData(listTripModel.categories as MutableList<ItemRecyclerCategoryModel.DataCategory>)
        recyclerVerticalBinding.rvItemHor.post {
            adapterCategory.notifyDataSetChanged()
        }
    }

    override fun onGetCategoryError() {
        recyclerVerticalBinding.swipeRefreshListTrip.isRefreshing = false
        ApplicationHelper.Helper.displayToast(activity!!, getString(R.string.error_get_list))
    }

    override fun onGetMealsSuccess(listTripModel: ItemRecyclerMealsModel) {
        recyclerVerticalBinding.swipeRefreshListTrip.isRefreshing = false
        adapterMeals.addData(listTripModel.meals as MutableList<ItemRecyclerMealsModel.DataMeals>)
        recyclerVerticalBinding.rvItemVer.post {
            adapterMeals.notifyDataSetChanged()
        }
    }

    override fun onGetMealsError() {
        recyclerVerticalBinding.swipeRefreshListTrip.isRefreshing = false
        ApplicationHelper.Helper.displayToast(activity!!, getString(R.string.error_get_list))
    }
}