package com.haidar.root.tugasvisionet001.networks

import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerCategoryModel
import io.reactivex.Observable

class CategoryResponse (private val apiService:RestApi) {

    fun getListTrip(): Observable<ItemRecyclerCategoryModel> {
        return apiService.getListCategory()
    }
}