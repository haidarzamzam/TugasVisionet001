package com.haidar.root.tugasvisionet001.networks

import com.haidar.root.tugasvisionet001.main.vertical.models.ItemRecyclerMealsModel
import io.reactivex.Observable

class MealsResponse (private val apiService:RestApi) {

    fun getListMeals(): Observable<ItemRecyclerMealsModel> {
        return apiService.getListMeals()
    }
}