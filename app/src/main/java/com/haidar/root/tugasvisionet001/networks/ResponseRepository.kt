package com.haidar.root.tugasvisionet001.networks

object ResponseRepository {

    fun provideListMealsRepository():MealsResponse{
        return MealsResponse(RestApi.Factory.create())
    }

    fun provideListCategoryRepository():CategoryResponse{
        return CategoryResponse(RestApi.Factory.create())
    }
}