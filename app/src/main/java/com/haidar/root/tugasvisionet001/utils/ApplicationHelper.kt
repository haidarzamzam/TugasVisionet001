package com.haidar.root.tugasvisionet001.utils

import android.app.Activity
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Patterns
import android.widget.Toast

class ApplicationHelper {

    companion object Helper{
        fun displayToast(activity: Activity, message: String){
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }

        fun fromHtml(input:String): Spanned? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                Html.fromHtml(input, Html.FROM_HTML_MODE_LEGACY)
            }else{
                Html.fromHtml(input)
            }
        }
    }

}